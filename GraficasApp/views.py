from django.shortcuts import render
from django.views.generic.edit import UpdateView, CreateView, DeleteView
from .models import *
from GraficasApp import forms
from django.urls import reverse_lazy
from django.http import JsonResponse
import json as simplejson

# Create your views here.



def base(request):
    return render(request, 'baseAdmin.html')

def graficas(request):
    e = Estudiante.objects.all()
    numeroDeEstudiantes = Estudiante.objects.all().count()
    context = {'estudiantes_list': e , 'numeroTotal':numeroDeEstudiantes}
    return render(request, 'graficas.html', context)


def estudiante_base(request):
    e = Estudiante.objects.all()
    context = {'estudiantes_list': e}
    return render(request, 'ListaEstudiantes.html', context)

class EstudianteCreate(CreateView):
    model = Estudiante
    form_class = forms.EstudianteForm
    template_name = 'estudianteCreate.html'
    success_url = reverse_lazy('lista_estudiantes')

class EstudianteEditar(UpdateView):
    model = Estudiante
    form_class = forms.EstudianteEditarForm
    template_name = 'estudianteEditar.html'
    success_url = reverse_lazy('lista_estudiantes')

class EstudianteDelete(DeleteView):
    model = Estudiante
    template_name = 'estudiante_confirm_delete.html'
    success_url = reverse_lazy('lista_estudiantes')


def grupoList(request):
    grupos = Grupo.objects.all()
    context = {'grupos':grupos}
    return render(request, 'gruposLista.html', context)

class GrupoCreate(CreateView):
    model = Grupo
    form_class = forms.GrupoForm
    template_name = 'GrupoCreate.html'
    success_url = reverse_lazy('lista_Grupos')

class GrupoEditar(UpdateView):
    model = Grupo
    form_class = forms.GrupoFormEditar
    template_name = 'GrupoEditar.html'
    success_url = reverse_lazy('lista_Grupos')

class GrupoDelete(DeleteView):
    model = Grupo
    template_name = 'Grupo_confirm_delete.html'
    success_url = reverse_lazy('lista_Grupos')

def estudiantesAgrupados(request, pk ,año):
    año = Grupo.objects.filter(año=año)
    grupo = Estudiante.objects.filter(grupo=pk)
    e = Estudiante.objects.all()
    context = {'grupo':grupo, 'año':año,'estudiantes_list': e}
    return render(request, 'EstudiantesAgrupados.html' , context )