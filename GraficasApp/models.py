from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator 
# Create your models here.

class Estudiante(models.Model):
   
    identificacion = models.CharField(max_length=100)
    nombres = models.CharField(max_length=100)
    apellidos = models.CharField(max_length=100)
    grupo = models.ForeignKey('Grupo',blank=True , null=True ,on_delete= models.CASCADE)

    fecha_saber11 = models.DateField( blank=True, null=True)
    ingles_saber11 = models.IntegerField(validators=[MaxValueValidator(300), MinValueValidator(0)] )
    lecturacritica_saber11 = models.IntegerField(validators=[MaxValueValidator(300), MinValueValidator(0)] )
    razonamiento_saber11  = models.IntegerField(validators=[MaxValueValidator(300), MinValueValidator(0)] )
    competencias_saber11  = models.IntegerField(validators=[MaxValueValidator(300), MinValueValidator(0)] )
    
    fecha_simulacro1 = models.DateField( blank=True, null=True)
    ingles_simulacro1 = models.IntegerField(validators=[MaxValueValidator(300), MinValueValidator(0)] ,blank=True,null=True)
    lecturacritica_simulacro1 = models.IntegerField(validators=[MaxValueValidator(300), MinValueValidator(0)] ,blank=True,null=True)
    razonamiento_simulacro1  = models.IntegerField(validators=[MaxValueValidator(300), MinValueValidator(0)] ,blank=True,null=True)
    competencias_simulacro1  = models.IntegerField(validators=[MaxValueValidator(300), MinValueValidator(0)] ,blank=True,null=True)
    escrito_simulacro1 = models.IntegerField(validators=[MaxValueValidator(300), MinValueValidator(0)] ,blank=True,null=True)
    formulacion_simulacro1 = models.IntegerField(validators=[MaxValueValidator(300), MinValueValidator(0)] ,blank=True,null=True)
    cientifico_simulacro1 = models.IntegerField(validators=[MaxValueValidator(300), MinValueValidator(0)] ,blank=True,null=True)
    diseño_simulacro1 = models.IntegerField(validators=[MaxValueValidator(300), MinValueValidator(0)] ,blank=True,null=True)

    fecha_simulacro2 = models.DateField( blank=True, null=True)
    ingles_simulacro2 = models.IntegerField(validators=[MaxValueValidator(300), MinValueValidator(0)] ,blank=True,null=True)
    lecturacritica_simulacro2 = models.IntegerField(validators=[MaxValueValidator(300), MinValueValidator(0)] ,blank=True,null=True)
    razonamiento_simulacro2   = models.IntegerField(validators=[MaxValueValidator(300), MinValueValidator(0)] ,blank=True,null=True)
    competencias_simulacro2   = models.IntegerField(validators=[MaxValueValidator(300), MinValueValidator(0)] ,blank=True,null=True)
    escrito_simulacro2 = models.IntegerField(validators=[MaxValueValidator(300), MinValueValidator(0)] ,blank=True,null=True)
    formulacion_simulacro2 = models.IntegerField(validators=[MaxValueValidator(300), MinValueValidator(0)] ,blank=True,null=True)
    cientifico_simulacro2 = models.IntegerField(validators=[MaxValueValidator(300), MinValueValidator(0)] ,blank=True,null=True)
    diseño_simulacro2 = models.IntegerField(validators=[MaxValueValidator(300), MinValueValidator(0)] ,blank=True,null=True)

    fecha_simulacro3 = models.DateField( blank=True, null=True)
    ingles_simulacro3 = models.IntegerField(validators=[MaxValueValidator(300), MinValueValidator(0)] ,blank=True,null=True)
    lecturacritica_simulacro3 = models.IntegerField(validators=[MaxValueValidator(300), MinValueValidator(0)] ,blank=True,null=True)
    razonamiento_simulacro3  = models.IntegerField(validators=[MaxValueValidator(300), MinValueValidator(0)] ,blank=True,null=True)
    competencias_simulacro3  = models.IntegerField(validators=[MaxValueValidator(300), MinValueValidator(0)] ,blank=True,null=True)
    escrito_simulacro3 = models.IntegerField(validators=[MaxValueValidator(300), MinValueValidator(0)] ,blank=True,null=True)
    formulacion_simulacro3 = models.IntegerField(validators=[MaxValueValidator(300), MinValueValidator(0)] ,blank=True,null=True)
    cientifico_simulacro3 = models.IntegerField(validators=[MaxValueValidator(300), MinValueValidator(0)] ,blank=True,null=True)
    diseño_simulacro3 = models.IntegerField(validators=[MaxValueValidator(300), MinValueValidator(0)] ,blank=True,null=True)

    fecha_saberPro = models.DateField( blank=True, null=True)
    ingles_saberPro = models.IntegerField(blank=True,validators=[MaxValueValidator(300), MinValueValidator(0)], null=True)
    lecturacritica_saberPro = models.IntegerField(validators=[MaxValueValidator(300), MinValueValidator(0)] ,blank=True,null=True)
    razonamiento_saberPro  = models.IntegerField(validators=[MaxValueValidator(300), MinValueValidator(0)] ,blank=True,null=True)
    competencias_saberPro  = models.IntegerField(validators=[MaxValueValidator(300), MinValueValidator(0)] ,blank=True,null=True)
    escrito_saberPro = models.IntegerField(validators=[MaxValueValidator(300), MinValueValidator(0)] ,blank=True,null=True)
    formulacion_saberPro = models.IntegerField(validators=[MaxValueValidator(300), MinValueValidator(0)] ,blank=True,null=True)
    cientifico_saberPro = models.IntegerField(validators=[MaxValueValidator(300), MinValueValidator(0)] ,blank=True,null=True)
    diseño_saberPro = models.IntegerField(validators=[MaxValueValidator(300), MinValueValidator(0)] ,blank=True,null=True)


class Grupo(models.Model):
    
    ingles = models.IntegerField(blank=True,validators=[MaxValueValidator(300), MinValueValidator(0)], null=True)
    lectura_critica= models.IntegerField(validators=[MaxValueValidator(300), MinValueValidator(0)] ,blank=True,null=True)
    razonamiento_cuantitativo  = models.IntegerField(validators=[MaxValueValidator(300), MinValueValidator(0)] ,blank=True,null=True)
    competencias_ciudadanas  = models.IntegerField(validators=[MaxValueValidator(300), MinValueValidator(0)] ,blank=True,null=True)
    escrito = models.IntegerField(validators=[MaxValueValidator(300), MinValueValidator(0)] ,blank=True,null=True)
    formulacion_proyectos = models.IntegerField(validators=[MaxValueValidator(300), MinValueValidator(0)] ,blank=True,null=True)
    pensamiento_cientifico = models.IntegerField(validators=[MaxValueValidator(300), MinValueValidator(0)] ,blank=True,null=True)
    diseño_software = models.IntegerField(validators=[MaxValueValidator(300), MinValueValidator(0)] ,blank=True,null=True)

    año = models.CharField(max_length=100)
    universidad = models.CharField(max_length=100)
    codigo = models.IntegerField(validators=[MinValueValidator(300)])

    def __str__(self):
        return self.año + " " + self.universidad