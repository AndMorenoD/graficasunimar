from django import forms
from .models import *


class GrupoForm(forms.ModelForm):
    class Meta:
        model = Grupo
        fields= '__all__'
        widgets = {
            'año': forms.TextInput(attrs={'class':'form-control'}),
            'universidad': forms.TextInput(attrs={'class':'form-control'}),
            'codigo': forms.NumberInput(attrs={'class':'form-control'}),

            'ingles': forms.NumberInput(attrs={'class':'form-control'}),
            'lectura_critica': forms.NumberInput(attrs={'class':'form-control'}),
            'razonamiento_cuantitativo': forms.NumberInput(attrs={'class':'form-control'}),
            'competencias_ciudadanas': forms.NumberInput(attrs={'class':'form-control'}),
            'escrito': forms.NumberInput(attrs={'class':'form-control'}),
            'formulacion_proyectos' :forms.NumberInput(attrs={'class':'form-control'}),
            'pensamiento_cientifico': forms.NumberInput(attrs={'class':'form-control'}),
            'diseño_software'  :forms.NumberInput(attrs={'class':'form-control'}),
            
        }

    def clean_codigo(self):
        codigo = self.cleaned_data.get("codigo")
        if Grupo.objects.filter(codigo=codigo).exists():
            raise forms.ValidationError("Ya existe un grupo con el codigo asignado.")
        return codigo

class GrupoFormEditar(forms.ModelForm):
    class Meta:
        model = Grupo
        fields = '__all__'
        widgets = {
            'año': forms.TextInput(attrs={'class':'form-control'}),
            'universidad': forms.TextInput(attrs={'class':'form-control'}),   
        }


class EstudianteForm(forms.ModelForm):
    
    class Meta:
        model = Estudiante
        fields= '__all__'
        widgets = {
            'identificacion': forms.TextInput(attrs={'class':'form-control'}),
            'grupo': forms.Select(attrs={'class':'form-control'}),
            'nombres': forms.TextInput(attrs={'class':'form-control'}),
            'apellidos': forms.TextInput(attrs={'class':'form-control'}),
            'fecha_saber11':forms.DateInput(attrs={'class':'form-control datepicker'}),
            'ingles_saber11': forms.NumberInput(attrs={'class':'form-control'}),
            'lecturacritica_saber11': forms.NumberInput(attrs={'class':'form-control'}),
            'razonamiento_saber11': forms.NumberInput(attrs={'class':'form-control'}),
            'competencias_saber11': forms.NumberInput(attrs={'class':'form-control'}),

            'fecha_simulacro1': forms.DateInput(attrs={'class':'form-control datepicker' }),

            'ingles_simulacro1': forms.NumberInput(attrs={'class':'form-control'}),
            'lecturacritica_simulacro1': forms.NumberInput(attrs={'class':'form-control'}),
            'razonamiento_simulacro1': forms.NumberInput(attrs={'class':'form-control'}),
            'competencias_simulacro1': forms.NumberInput(attrs={'class':'form-control'}),
            'escrito_simulacro1': forms.NumberInput(attrs={'class':'form-control'}),
            'formulacion_simulacro1' :forms.NumberInput(attrs={'class':'form-control'}),
            'cientifico_simulacro1': forms.NumberInput(attrs={'class':'form-control'}),
            'diseño_simulacro1'  :forms.NumberInput(attrs={'class':'form-control'}),

            'fecha_simulacro2': forms.DateInput(attrs={'class':'form-control datepicker' }),

            'ingles_simulacro2': forms.NumberInput(attrs={'class':'form-control'}),
            'lecturacritica_simulacro2': forms.NumberInput(attrs={'class':'form-control'}),
            'razonamiento_simulacro2': forms.NumberInput(attrs={'class':'form-control'}),
            'competencias_simulacro2': forms.NumberInput(attrs={'class':'form-control'}),
            'escrito_simulacro2': forms.NumberInput(attrs={'class':'form-control'}),
            'formulacion_simulacro2' :forms.NumberInput(attrs={'class':'form-control'}),
            'cientifico_simulacro2': forms.NumberInput(attrs={'class':'form-control'}),
            'diseño_simulacro2'  :forms.NumberInput(attrs={'class':'form-control'}),
            'fecha_simulacro3': forms.DateInput(attrs={'class':'form-control datepicker' }),

            'ingles_simulacro3': forms.NumberInput(attrs={'class':'form-control'}),
            'lecturacritica_simulacro3': forms.NumberInput(attrs={'class':'form-control'}),
            'razonamiento_simulacro3': forms.NumberInput(attrs={'class':'form-control'}),
            'competencias_simulacro3': forms.NumberInput(attrs={'class':'form-control'}),
            'escrito_simulacro3': forms.NumberInput(attrs={'class':'form-control'}),
            'formulacion_simulacro3' :forms.NumberInput(attrs={'class':'form-control'}),
            'cientifico_simulacro3': forms.NumberInput(attrs={'class':'form-control'}),
            'diseño_simulacro3'  :forms.NumberInput(attrs={'class':'form-control'}),
            'fecha_saberPro': forms.DateInput(attrs={'class':'form-control datepicker' }),

            'ingles_saberPro': forms.NumberInput(attrs={'class':'form-control'}),
            'lecturacritica_saberPro': forms.NumberInput(attrs={'class':'form-control'}),
            'razonamiento_saberPro': forms.NumberInput(attrs={'class':'form-control'}),
            'competencias_saberPro': forms.NumberInput(attrs={'class':'form-control'}),
            'escrito_saberPro': forms.NumberInput(attrs={'class':'form-control'}),
            'formulacion_saberPro' :forms.NumberInput(attrs={'class':'form-control'}),
            'cientifico_saberPro': forms.NumberInput(attrs={'class':'form-control'}),
            'diseño_saberPro'  :forms.NumberInput(attrs={'class':'form-control'}),
        }
    
    def clean_identificacion(self):
        identificacion = self.cleaned_data.get("identificacion")
        if Estudiante.objects.filter(identificacion=identificacion).exists():
            raise forms.ValidationError("Ya existe un estudiante con la identificacion ingresada.")
        return identificacion




class EstudianteEditarForm(forms.ModelForm):
    
    class Meta:
        model = Estudiante
        fields= '__all__'
        widgets = {
            'identificacion': forms.TextInput(attrs={'class':'form-control'}),
            'nombres': forms.TextInput(attrs={'class':'form-control'}),
            'apellidos': forms.TextInput(attrs={'class':'form-control'}),
            'grupo': forms.Select(attrs={'class':'form-control'}),
            'fecha_saber11': forms.DateInput(attrs={'class':'form-control datepicker' }),
            'ingles_saber11': forms.NumberInput(attrs={'class':'form-control'}),
            'lecturacritica_saber11': forms.NumberInput(attrs={'class':'form-control'}),
            'razonamiento_saber11': forms.NumberInput(attrs={'class':'form-control'}),
            'competencias_saber11': forms.NumberInput(attrs={'class':'form-control'}),
            'fecha_simulacro1': forms.DateInput(attrs={'class':'form-control datepicker' }),          
            'ingles_simulacro1': forms.NumberInput(attrs={'class':'form-control'}),
            'lecturacritica_simulacro1': forms.NumberInput(attrs={'class':'form-control'}),
            'razonamiento_simulacro1': forms.NumberInput(attrs={'class':'form-control'}),
            'competencias_simulacro1': forms.NumberInput(attrs={'class':'form-control'}),
            'escrito_simulacro1': forms.NumberInput(attrs={'class':'form-control'}),
            'formulacion_simulacro1' :forms.NumberInput(attrs={'class':'form-control'}),
            'cientifico_simulacro1': forms.NumberInput(attrs={'class':'form-control'}),
            'diseño_simulacro1'  :forms.NumberInput(attrs={'class':'form-control'}),

            'fecha_simulacro2': forms.DateInput(attrs={'class':'form-control datepicker' }),
            'ingles_simulacro2': forms.NumberInput(attrs={'class':'form-control'}),
            'lecturacritica_simulacro2': forms.NumberInput(attrs={'class':'form-control'}),
            'razonamiento_simulacro2': forms.NumberInput(attrs={'class':'form-control'}),
            'competencias_simulacro2': forms.NumberInput(attrs={'class':'form-control'}),
            'escrito_simulacro2': forms.NumberInput(attrs={'class':'form-control'}),
            'formulacion_simulacro2' :forms.NumberInput(attrs={'class':'form-control'}),
            'cientifico_simulacro2': forms.NumberInput(attrs={'class':'form-control'}),
            'diseño_simulacro2'  :forms.NumberInput(attrs={'class':'form-control'}),
            'fecha_simulacro3': forms.DateInput(attrs={'class':'form-control datepicker' }),
            'ingles_simulacro3': forms.NumberInput(attrs={'class':'form-control'}),
            'lecturacritica_simulacro3': forms.NumberInput(attrs={'class':'form-control'}),
            'razonamiento_simulacro3': forms.NumberInput(attrs={'class':'form-control'}),
            'competencias_simulacro3': forms.NumberInput(attrs={'class':'form-control'}),
            'escrito_simulacro3': forms.NumberInput(attrs={'class':'form-control'}),
            'formulacion_simulacro3' :forms.NumberInput(attrs={'class':'form-control'}),
            'cientifico_simulacro3': forms.NumberInput(attrs={'class':'form-control'}),
            'diseño_simulacro3'  :forms.NumberInput(attrs={'class':'form-control'}),
            'fecha_saberPro': forms.DateInput(attrs={'class':'form-control datepicker' }),
            'ingles_saberPro': forms.NumberInput(attrs={'class':'form-control'}),
            'lecturacritica_saberPro': forms.NumberInput(attrs={'class':'form-control'}),
            'razonamiento_saberPro': forms.NumberInput(attrs={'class':'form-control'}),
            'competencias_saberPro': forms.NumberInput(attrs={'class':'form-control'}),
            'escrito_saberPro': forms.NumberInput(attrs={'class':'form-control'}),
            'formulacion_saberPro' :forms.NumberInput(attrs={'class':'form-control'}),
            'cientifico_saberPro': forms.NumberInput(attrs={'class':'form-control'}),
            'diseño_saberPro'  :forms.NumberInput(attrs={'class':'form-control'}),
        }