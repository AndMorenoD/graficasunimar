// Call the dataTables jQuery plugin
$(document).ready(function() {
  $('#dataTable').DataTable({
    "pagingType": "full_numbers",
    responsive: true,
    language: {
      infoEmpty: "No existen estudiantes registrados.",
      emptyTable: "Ningun estudiante ha sido agregado.",
      infoFiltered: " - Filtrado de _MAX_ resultados",
      search: "_INPUT_",
      searchPlaceholder: "Buscar",
      info: "Mostrando _START_ de _END_ estudiantes - Numero de estudiantes: _TOTAL_ ",
      paginate: {
        "first": "Inicio",
        "previous":"Anterior",
        "next":"Siguiente",
        "last":"Final"
      },
      lengthMenu: 'Mostrar <select>'+
      '<option value="5">5</option>'+
      '<option value="10">10</option>'+
      '<option value="20">20</option>'+
      '<option value="30">30</option>'+
      '<option value="40">40</option>'+
      '<option value="50">50</option>'+
      '<option value="-1">Todos</option>'+
      '</select> estudiantes'
    }
    
  });
  $('#dataTableComparacion1').DataTable({
    "pagingType": "full_numbers",
    responsive: true,
    language: {
      infoEmpty: "No existen grupos de referencia registrados.",
      emptyTable: "Ningun grupo de referencia ha sido agregado.",
      infoFiltered: " - Filtrado de _MAX_ resultados",
      search: "_INPUT_",
      searchPlaceholder: "Buscar",
      info: "Mostrando _START_ de _END_ grupos - Numero de grupos: _TOTAL_ ",
      paginate: {
        "first": "Inicio",
        "previous":"Anterior",
        "next":"Siguiente",
        "last":"Final"
      },
      lengthMenu: 'Mostrar <select>'+
      '<option value="1" selected>1</option>'+
      '<option value="10">10</option>'+
      '<option value="20">20</option>'+
      '<option value="30">30</option>'+
      '<option value="40">40</option>'+
      '<option value="50">50</option>'+
      '<option value="-1">Todos</option>'+
      '</select> grupos'
    }
    
  });
  $('#dataTableComparacion2').DataTable({
    "pagingType": "full_numbers",
    responsive: true,
    language: {
      infoEmpty: "No existen grupos de referencia registrados.",
      emptyTable: "Ningun grupo de referencia ha sido agregado.",
      infoFiltered: " - Filtrado de _MAX_ resultados",
      search: "_INPUT_",
      searchPlaceholder: "Buscar",
      info: "Mostrando _START_ de _END_ grupos - Numero de grupos: _TOTAL_ ",
      paginate: {
        "first": "Inicio",
        "previous":"Anterior",
        "next":"Siguiente",
        "last":"Final"
      },
      lengthMenu: 'Mostrar <select>'+
      '<option value="1">1</option>'+
      '<option value="10">10</option>'+
      '<option value="20">20</option>'+
      '<option value="30">30</option>'+
      '<option value="40">40</option>'+
      '<option value="50">50</option>'+
      '<option value="-1">Todos</option>'+
      '</select> grupos'
    }
    
  });
});

