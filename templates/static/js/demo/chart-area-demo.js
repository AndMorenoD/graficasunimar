// Set new default font family and font color to mimic Bootstrap's default styling
Chart.defaults.global.defaultFontFamily = 'Nunito', '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
Chart.defaults.global.defaultFontColor = '#858796';

window.chartColors = {
	red: 'rgb(255, 99, 132)',
	orange: 'rgb(255, 159, 64)',
	yellow: 'rgb(255, 205, 86)',
	green: 'rgb(75, 250, 192)',
	blue: 'rgb(54, 162, 235)',
	purple: 'rgb(153, 102, 255)',
	grey: 'rgb(201, 203, 207)'
};
var cantidadEstudiantes = parseInt( document.getElementById('cantidad').innerText);

for (var i = 0; i < cantidadEstudiantes; i++) {
	var c = i+1
	var tablas = "datos" + c 
	var canvas = "canvas" + c
	
	var t = document.getElementById(tablas);
	var f = t.getElementsByTagName('tr');
	var td_saber11 = f[0].getElementsByTagName('td');
	var td_simulacro1 = f[1].getElementsByTagName('td');
	var td_simulacro2 = f[2].getElementsByTagName('td');
	var td_simulacro3 = f[3].getElementsByTagName('td');
	var td_saberPro = f[4].getElementsByTagName('td');
	
	var puntaje_saber11 = [];
	var puntaje_simulacro1 = [];
	var puntaje_simulacro2 = [];
	var puntaje_simulacro3 = [];
	var puntaje_saberPro = [];
	
	for (var j = 0; j < td_saber11.length; j++) {
		var datos = td_saber11[j].innerText
		puntaje_saber11.push(datos)
	}
	
	for (var j = 0; j < td_simulacro1.length; j++) {
		var datos = td_simulacro1[j].innerText
		puntaje_simulacro1.push(datos)
	}
	
	for (var j = 0; j < td_simulacro1.length; j++) {
		var datos = td_simulacro1[j].innerText
		puntaje_simulacro2.push(datos)
	}
	
	for (var j = 0; j < td_simulacro3.length; j++) {
		var datos = td_simulacro3[j].innerText
		puntaje_simulacro3.push(datos)
	}
	
	for (var j = 0; j < td_saberPro.length; j++) {
		var datos = td_saberPro[j].innerText
		puntaje_saberPro.push(datos)
	}
	
	var config = {
		type: 'line',
		data: {
			labels: ['\'Ingles','\'Lec.Critica','\'Razonamiento','\'Comp.Ciudadana','\'Com.Escrita','\'Formulacion','\'Pto.Cientifico','\'Dis.Software'],
			datasets: [{
				label: 'Saber 11',
				backgroundColor: window.chartColors.blue,
				borderColor: window.chartColors.blue,
				data: puntaje_saber11,
				
				fill: false,
			}, {
				label: 'Primer Simulacro',
				fill: false,
				backgroundColor: window.chartColors.purple,
				borderColor: window.chartColors.purple,
				data: puntaje_simulacro1,
			}, {
				label: 'Segundo Simulacro',
				borderColor: window.chartColors.red,
				backgroundColor: window.chartColors.red,
				fill: false,
				data: puntaje_simulacro2,
			}, {
				label: 'Tercer Simulacro',
				borderColor: window.chartColors.orange,
				backgroundColor: window.chartColors.orange,
				fill: false,
				data: puntaje_simulacro3,
				
			}, {
				label: 'Saber Pro',
				borderColor: window.chartColors.green,
				backgroundColor: window.chartColors.green,
				fill: false,
				data: puntaje_saberPro,
				
			}],
		},
		options: {
			responsive: true,
			title: {
				display: true,
				text: 'Resultados de las pruebas de estado.'
			},
			tooltips: {
				mode: 'index',
				intersect: false,
			},
			hover: {
				mode: 'nearest',
				intersect: true
			},
			scales: {
				xAxes: [{
					display: true,
					scaleLabel: {
						display: true,
						labelString: 'Nombres de las competencias'
					}
				}],
				yAxes: [{
					display: true,
					scaleLabel: {
						display: true,
						labelString: 'Puntaje de 0 a 300'
					}
				}]
			}
		}
	};
	
	
	window.myLine = new Chart(document.getElementById(canvas).getContext('2d'), config);
	
	
	
	
}



